# World APN list
PRODUCT_COPY_FILES += \
    vendor/illusion/prebuilt/common/etc/apns-conf.xml:system/etc/apns-conf.xml

# Telephony packages
PRODUCT_PACKAGES += \
    CellBroadcastReceiver \
    Mms \
    Stk
