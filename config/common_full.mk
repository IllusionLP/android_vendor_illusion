# Inherit additional stuff
$(call inherit-product, vendor/illusion/config/common.mk)

# Optional packages
PRODUCT_PACKAGES += \
    LiveWallpapers \
    LiveWallpapersPicker \
    PhaseBeam

